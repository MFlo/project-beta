from django.contrib import admin
from .models import AutomobileVO, Technician, Appointment



#register your models here
@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass
