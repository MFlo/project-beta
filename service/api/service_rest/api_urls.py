from django.urls import path
from .api_views import (api_show_appointment, api_show_technician, api_list_appointments, api_list_technicians)


urlpatterns = [
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<str:vin>/", api_list_appointments, name="vin_appointments"),
    path("appointments/detail/<int:id>/", api_show_appointment, name="show_appointment"),
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:id>/", api_show_technician, name="show_technician"),
]


