import React, { useState, useEffect } from 'react';

const SalesList = () => {
  const [sales, setSales] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [vin, setVin] = useState(''); // State to hold the VIN filter

  useEffect(() => {
    const fetchSales = async () => {
      setIsLoading(true);
      try {
        // Include the VIN in the API request
        const url = vin
          ? `http://localhost:8090/api/sales/?vin=${vin}`
          : 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Could not fetch sales data!');
        }
        const data = await response.json();
        setSales(data.sales);
      } catch (error) {
        setError(error);
      }
      setIsLoading(false);
    };

    fetchSales();
  }, [vin]); // Depend on the VIN so the effect runs when it changes

  // Event handler for VIN input change
  const handleVinChange = (event) => {
    setVin(event.target.value);
  };

  return (
    <div>
      <input
        type="text"
        value={vin}
        onChange={handleVinChange}
        placeholder="Filter by VIN"
      />
      {isLoading && <p>Loading sales data...</p>}
      {error && <p>Error: {error.message}</p>}
      {!isLoading && !error && sales.length > 0 && (
        <table>
          <thead>
            <tr>
              <th>VIN</th>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Price</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sale) => (
              <tr key={sale.id}>
                <td>{sale.automobile.vin}</td>
                <td>{sale.salesperson.name}</td>
                <td>{sale.customer.name}</td>
                <td>${sale.price}</td>
                <td>{new Date(sale.date).toLocaleDateString()}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
      {sales.length === 0 && !isLoading && <p>No sales found.</p>}
    </div>
  );
};

export default SalesList;
