import React, { useState } from 'react';

const AddSalesperson = () => {
  // create hooks for each input field like customerform
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employeeNumber: '',
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        setError('');
        setSuccess('');

        fetch('http://localhost:8090/api/salespeople/', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        first_name: formData.first_name,
        last_name: formData.last_name,
        employee_id: formData.employeeNumber,
    }),
})
.then(response => {
    setLoading(false);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.json();
})
.then(data => {
    setSuccess('Salesperson created successfully');
    // Clear the form data after submission
    setFormData({ first_name: '', last_name: '', employeeNumber: '' });
})
.catch(error => {
    setError('Failed to create salesperson: ' + error.message);
    // Handle error
});
    };

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Sales Person!</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.employeeNumber} placeholder="Employee Number" required type="text" name="employeeNumber" className="form-control" />
                <label htmlFor="employeeNumber">Employee Number</label>
              </div>
              <button type="submit" disabled={loading} className="btn btn-primary">
                {loading ? 'Creating...' : 'Create'}
              </button>
            </form>
            {error && <div className="alert alert-danger">{error}</div>}
            {success && <div className="alert alert-success">{success}</div>}
          </div>
        </div>
      </div>
    );
};

export default AddSalesperson;
