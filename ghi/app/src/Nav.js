import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  MDBNavbar,
  MDBContainer,
  MDBNavbarBrand,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
} from 'mdb-react-ui-kit';

function Nav() {
  return (
    <MDBNavbar expand='lg' dark bgColor='dark'>
      <MDBContainer fluid>
        <MDBNavbarBrand href='/'>CarCar</MDBNavbarBrand>
        <MDBNavbarToggler
          aria-controls='navbarResponsive'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <i className="fas fa-bars"></i>
        </MDBNavbarToggler>
        <MDBCollapse navbar id="navbarResponsive">
          <MDBNavbarNav right fullWidth={false} className='mb-2 mb-lg-0'>

            {/* Services Dropdown */}
            <MDBNavbarItem>
              <MDBDropdown>
                <MDBDropdownToggle tag='a' className='nav-link'>
                  Services
                </MDBDropdownToggle>
                <MDBDropdownMenu>
                  <MDBDropdownItem>{/* ... other dropdown items ... */}
                    <NavLink className="dropdown-item" to="/technician/new">New Technician</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/appointment/new">Make a Service Appointment</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/appointments/list">Appointment List</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/services/new">Service History</NavLink>
                  </MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavbarItem>

            {/* Sales Dropdown */}
            <MDBNavbarItem>
              <MDBDropdown>
                <MDBDropdownToggle tag='a' className='nav-link'>
                  Sales
                </MDBDropdownToggle>
                <MDBDropdownMenu>
                  <MDBDropdownItem> {/* ... sales dropdown items ... */}
                    <NavLink className="dropdown-item" to="/customers/new/">Add a Customer</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/salesperson/new/">Add a Sales Person</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/salesrecords/new/">Add a Sales Record</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/salesrecords/">Sales Records List</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/salesperson/history/">Sales Person History</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/customer/list">Preffered Customer List</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/sales_person/list">List Sales People</NavLink>
                  </MDBDropdownItem>


                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavbarItem>

            {/* Inventories Dropdown */}
            <MDBNavbarItem>
              <MDBDropdown>
                <MDBDropdownToggle tag='a' className='nav-link'>
                  Inventories
                </MDBDropdownToggle>
                <MDBDropdownMenu>
                  <MDBDropdownItem> {/* ... inventories dropdown items ... */}
                    <NavLink className="dropdown-item" to="/manufacturers/new/">Add a Manufacturer</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/manufacturers/">Manufacturer List</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/models/new">Add a Vehicle Models</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/models/">Vehicle Models List</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/automobiles/new">Add an Automobile</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="/automobiles/all">Automobiles List</NavLink>
                  </MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavbarItem>
          </MDBNavbarNav>

          {/* Notifications */}
          <MDBNavbarItem>
            <MDBDropdown>
              <MDBDropdownToggle tag='a' className='nav-link'>
                <i className="fas fa-bell">Hey!</i>
              </MDBDropdownToggle>
              <MDBDropdownMenu>
                <MDBDropdownItem>
                  <NavLink className="dropdown-item" to="">Some News</NavLink>
                </MDBDropdownItem>
                  <MDBDropdownItem>{/* ... other notifications ... */}
                    <NavLink className="dropdown-item" to="">Another News</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="">Something else here</NavLink>
                  </MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavbarItem>

          {/* Avatar */}
          <MDBNavbarItem>
            <MDBDropdown>
              <MDBDropdownToggle tag='a' className='nav-link'>
                <i className="fas fa-user">You🖐🏽</i>
              </MDBDropdownToggle>
              <MDBDropdownMenu>
                  <MDBDropdownItem>{/* ... avatar dropdown items ... */}
                    <NavLink className="dropdown-item" to="">Profile</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="">Settings</NavLink>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <NavLink className="dropdown-item" to="">Logout</NavLink>
                  </MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavbarItem>
        </MDBCollapse>
      </MDBContainer>
    </MDBNavbar>
  );
}

export default Nav;
