import React, { useState } from 'react';

const CustomerForm = () => {
  // State hooks for each input field
  const [formData, setFormData] = useState({
      first_name: '',
      last_name: '',
      address: '',
      phoneNumber: '',
  })
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  // Function to handle the form submission
  const handleSubmit = (e) => {
    e.preventDefault(); // Prevent the default form submit action
    setLoading(true);
    setError('');
    setSuccess('');

    // Simple POST request with a JSON body using fetch
    fetch('http://localhost:8090/api/customers/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        first_name: formData.first_name,
        last_name: formData.last_name,
        address: formData.address,
        phone_number: formData.phoneNumber,
      }),
})
.then(response => {
    setLoading(false);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.json();
})
.then(data => {
    setSuccess('Customer created successfully');
      //Reset form fields
})
.catch(error => {
    setError('Failed to create customer: ' + error.message);
});
    };

  // JSX for the form
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Add a New Customer</h2>
          <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.address} placeholder="Address" required type="text" name="address" className="form-control" />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={formData.phoneNumber} placeholder="Phone Number" required type="text" name="phoneNumber" className="form-control" />
                <label htmlFor="phoneNumber">Phone Number</label>
              </div>
              <button type="submit" disabled={loading} className="btn btn-primary">
                {loading ? 'Creating...' : 'Create'}
              </button>
            </form>
            {error && <div className="alert alert-danger">{error}</div>}
            {success && <div className="alert alert-success">{success}</div>}
          </div>
        </div>
      </div>
    );
};

export default CustomerForm;
