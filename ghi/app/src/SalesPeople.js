import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8090/api/salespeople/')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        setSalespeople(data.salespeople || []);
      })
      .catch((error) => {
        console.error('Error fetching salespeople:', error);
      });
  }, []);

  const handleDelete = (salespersonId) => {
    const isConfirmed = window.confirm("Are you sure you want to delete this salesperson?");
    if (isConfirmed) {
      fetch(`http://localhost:8090/api/salespeople/${salespersonId}/`, {  // Note the backticks here
        method: 'DELETE',
      })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not OK');
        }
        setSalespeople(salespeople.filter((salesperson) => salesperson.id !== salespersonId));
      })
      .catch((error) => {
        console.error('Error deleting salesperson:', error);
      });
    }
  }

  return (
    <div className="row">
      <div className="col-12">
        <h1>Salespeople</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Number</th>
              <th>Employee ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {salespeople.map((salesperson) => (
              <tr key={salesperson.id}>
                <td>{salesperson.id}</td>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>
                  <button onClick={() => handleDelete(salesperson.id)}>Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default SalespeopleList;
