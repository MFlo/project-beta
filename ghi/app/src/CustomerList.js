import React, { useState, useEffect } from 'react';

function CustomerList() {
  const [customers, setCustomers] = useState([]); // Initial empty array of customers

  // Function to fetch customer data from the backend
  useEffect(() => {
    fetch('http://localhost:8090/api/customers/')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Netowrk response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        setCustomers(data.customers || []); // Update state with fetched customers
      })
      .catch((error) => {
        console.error('Error fetching customers:', error);
      });
  }, []); // The empty array ensures this effect runs once on mount

  const handleDelete = (customerId) => {
    //Show confirm window
    const isConfirmed = window.confirm("Are you sure you want to delete this customer?");
    //Proceed only if the user confirmed the action
    if (isConfirmed) {
    fetch(`http://localhost:8090/api/customers/${customerId}/`, {
      method: 'DELETE',
  })
  .then((response) => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    //Remove the customer from the state
    setCustomers(customers.filter((customer) => customer.id !== customerId));
  })
  .catch((error) => {
    console.error('Error deleting customer:', error);
  });
  }
  };
  // JSX for the customer list
  return (
    <div className="row">
      <div className="col-12">
        <h1>Customers</h1>
        <table className="table">
          <thead>
            <tr>
              <th>Customer ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Phone Number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {customers.map((customer) => (
              <tr key={customer.id}>
                <td>{customer.id}</td>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
                <td>
                  <button onClick={() => handleDelete(customer.id)}>Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default CustomerList;
