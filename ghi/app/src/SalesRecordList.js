import React, { useEffect, useState } from 'react';

function SalesRecordsList() {
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]); // Use filteredSales for display
  const [showSold, setShowSold] = useState(true); // Separate filter for sold/unsold
  const [searchVin, setSearchVin] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchSales = async () => {
      setIsLoading(true);
      setError(null);

      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (!response.ok) {
          throw new Error('Failed to fetch sales');
        }
        const data = await response.json();
        setSales(data.sales);
        setFilteredSales(data.sales); // Update filteredSales initially
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchSales();
  }, []);

  const cancel = async (sales) => {

    const url = `http://localhost:8080/api/appointments/${sales.vin}`
    const fetchConfig = {
        method: 'delete',
        headers: {
            'Content-Type': 'sales/json',
        },
    };
    const response = await fetch(url, fetchConfig);
    const data = await response.json();
    console.log(data)
}

const finish = async (sales) => {
    const url = `http://localhost:8080/api/appointments/${sales.vin}`
    const fetchConfig = {
        method: 'delete',
        headers: {
            'Content-Type': 'sales/json',
        },
    };
    const response = await fetch(url, fetchConfig);
    const data = await response.json();
    console.log(data)
}


  const handleSearch = async (event) => {
    setSearchVin(event.target.value);
    const results = sales.filter((sale) => sale.vin.includes(searchVin));
    setFilteredSales(results);
  };

  const toggleSoldStatus = () => {
    setShowSold(!showSold);
  };

  const filteredData = filteredSales.filter((sale) => showSold === sale.sold); // Filter based on sold status


  return (
    <div>
      {isLoading && <p>Loading sales data...</p>}
      {error && <p>Error: {error.message}</p>}
      {filteredData.length > 0 && (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Employee Number</th>
              <th>Customer's Name</th>
              <th>Automobile VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map((sale) => (
              <tr

  key={sale.vin}>


  <td>{sale.sales_person.name}</td>


  <td>{sale.sales_person.employee_number}</td>


  <td>{sale.customer.name}</td>


  <td>{sale.automobile.vin}</td>


  <td>{sale.price}</td>

  <td><button type="button" className="btn btn-danger" onClick={()=>cancel(sale)}>Cancel</button></td>

  <td><button type="button" className="btn btn-primary" onClick={()=>finish(sale)}>Finish</button></td>

  </tr>
            ))}
          </tbody>


  </table>
      )}
      {filteredData.length === 0 && !isLoading && <p>No sales found.</p>}
      <div>
        <input type="text" placeholder="Search by VIN" value={searchVin} onChange={handleSearch} />
        <button onClick={toggleSoldStatus}>
          {showSold ? 'Show Unsold' : 'Show Sold'}
        </button>
      </div>
    </div>
  );
}

export default SalesRecordsList;
