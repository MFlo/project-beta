import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import './mdb5-free-standard/css/mdb.min.css';


const rootElement = document.getElementById("root");
const root = ReactDOM.createRoot(rootElement);

function Main() {
  const [data, setData] = useState({ loaded: false, error: null });

  useEffect(() => {
    async function loadItems() {
      try {
        const manufacturerResponse = await fetch("http://localhost:8100/api/manufacturers/");
        const vehicleResponse = await fetch("http://localhost:8100/api/models/");
        const automobileResponse = await fetch("http://localhost:8100/api/automobiles/");

        if (!manufacturerResponse.ok || !vehicleResponse.ok || !automobileResponse.ok) {
          throw new Error('One or more requests failed!');
        }

        const manufacturerData = await manufacturerResponse.json();
        const vehicleData = await vehicleResponse.json();
        const automobileData = await automobileResponse.json();

        setData({ loaded: true, data: { manufacturerData, vehicleData, automobileData } });
      } catch (error) {
        setData({ loaded: true, error });
      }
    }

    loadItems();
  }, []);

  if (!data.loaded) {
    return <div>Loading...</div>; // Consider replacing this with a spinner or a skeleton component
  }

  if (data.error) {
    return <div>Error: {data.error.message}</div>; // Display error message
  }

  return (
    <React.StrictMode>
      <App {...data.data} />
    </React.StrictMode>
  );
}

root.render(<Main />);
