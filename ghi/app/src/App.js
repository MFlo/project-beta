import React from 'react';
// import './mdb5-free-standard/css/mdb.min.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalesPersonHistoryList from "./SalesPersonHistoryList";
import SalesPersonForm from "./AddSalesperson";
import SalesPeople from "./SalesPeople";
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import SalesRecordForm from "./RecordSaleForm";
import SalesRecordList from "./SalesRecordList";
import VehicleForm from "./VehicleForm";
import VehicleList from "./VehicleList";
import AutomobileForm from "./AutomobileForm";
import AutomobileList from "./AutomobileList";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import TechnicianForm from "./TechnicianForm";
import ServiceHistory from "./ServiceHistory";
import Footer from "./Footer";


function App(props) {
  // Reference to inventory lsit
  const manufacturer_list = props.manufacturerData["manufacturers"];
  const vehicle_list = props.vehicleData["models"];
  const automobile_list = props.automobileData["autos"];

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* Sales */}
          <Route path="/" element={<MainPage />} />
          <Route path="customer/new" element={<CustomerForm />} />
          <Route path="customer/list" element={<CustomerList />} />
          <Route path="sales_record/new" element={<SalesRecordForm automobile_list={automobile_list} />} />
          <Route path="sales_record/list" element={<SalesRecordList />} />
          <Route path="sales_person/new" element={<SalesPersonForm />} />
          <Route path="sales_person/list" element={<SalesPeople />} />
          <Route path="sales_person/history" element={<SalesPersonHistoryList />} />

          {/* Services */}
          <Route path="/" element={<MainPage />} />
          <Route path="appointment" element={<AppointmentList />} />
          <Route path="appointment/new" element={<AppointmentForm />} />
          <Route path="technician" element={<TechnicianForm />} />
          <Route path="service_history" element={<ServiceHistory />} />

          {/* Inventory */}
          <Route path="/" element={<MainPage />} />
          <Route path="vehicle" element={<VehicleList vehicle_list={vehicle_list} />} />
          <Route path="vehicle/new" element={<VehicleForm manufacturer_list={manufacturer_list} />} />
          <Route path="automobile" element={<AutomobileList />} />
          <Route path="automobile/new" element={<AutomobileForm />} />
          <Route path="manufacturer" element={<ManufacturerList manufacturer_list={manufacturer_list} />} />
          <Route path="manufacturer/new" element={<ManufacturerForm />} />
        </Routes>
        <Footer path="footer" element={<Footer />} />
      </div>
    </BrowserRouter>
  );
}

export default App;
