import React from 'react';
import { NavLink } from 'react-router-dom';
import './index.css';
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCardImage,
  MDBRipple,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
} from 'mdb-react-ui-kit';
import Inventory_image from './images/pexels-rangga-aditya-armien-235222.jpg';
import Sales_image from './images/pexels-antoni-shkraba-7144253.jpg';
import Services_image from './images/pexels-andrea-piacquadio-3806288.jpg';

function MainPage() {
  return (
    <div className="container-fluid dark-theme">
      <header>
      {/* -- Jumbotron -- */}
      <div className="p-5 text-center bg-body-tertiary mb-5">
        {/* Jumbotron content */}
        <h1 className="mb-3 text-white">We're going places</h1>
            <h4 className="mb-4">...and it's awesome!</h4>
            <a className="btn btn-primary btn-lg" href="/customer/new" role="button">Come drive with us</a>
            <h4 className="mb-4 text-red">Vroom! Vroom!</h4>
      </div>
      </header>
      {/* Cards Row */}
      <MDBRow>
        {/* Inventory Card with Dropdown */}
        <MDBCol md="4">
          <CardWithDropdown
            image={Inventory_image}
            title="Automobile Inventory"
            text="Manage manufacturers, vehicle models, and automobiles."
            dropdownLinks={[
              { to: "/vehicle", text: "Car Model List" },
              { to: "/vehicle/new", text: "ADD Car Model" },
              { to: "/automobile", text: "Automobile List"},
              { to: "/automobile/new", text: "ADD an Automobile"},
              { to: "/manufacturer", text: "Manufacturer List"},
              { to: "/manufacturer/new", text: "ADD a Manufacturer"},
              // ...other links
            ]}
          />
        </MDBCol>

        {/* Sales Card with Dropdown */}
        <MDBCol md="4">
          <CardWithDropdown
            image={Sales_image}
            title="Automobile Sales"
            text="Manage customers, sales records, and salesperson information."
            dropdownLinks={[
              { to: "/customer/new", text: "ADD a Customer" },
              { to: "/customer/list", text: "Preferred Customer List" },
              { to: "/sales_record/new", text: "Add a Sales Record"},
              { to: "/sales_record/list", text: "Sales Record List"},
              { to: "/sales_person/new", text: "Add a Sales Person"},
              { to: "/sales_person/list", text: "List Sales People"},
              { to: "/sales_person/history", text: "Sales Person History"},
              // ...other links
            ]}
          />
        </MDBCol>

        {/* Services Card with Dropdown */}
        <MDBCol md="4">
          <CardWithDropdown
            image={Services_image}
            title="Automobile Services"
            text="Schedule service appointments, create technicians, and view appointments."
            dropdownLinks={[
              { to: "/appointment", text: "Appointment List" },
              { to: "/appointment/new", text: "Make Appointment" },
              { to: "/technician", text: "Add a Technician"},
              { to: "/service_history", text: "Customer Service History"},
              // ...other links
            ]}
          />
        </MDBCol>
      </MDBRow>
    </div>
  );
}

function CardWithDropdown({ image, title, text, dropdownLinks }) {
  return (
    <MDBCard className="mb-5 dark-card">
      <MDBRipple rippleColor="light" rippleTag="div" className="bg-image hover-overlay">
        <MDBCardImage src={image} alt={title} fluid />
        <a>
          <div className="mask" style={{ backgroundColor: 'grey' }}></div>
        </a>
      </MDBRipple>
      <MDBCardBody>
        <MDBCardTitle>{title}</MDBCardTitle>
        <MDBCardText>{text}</MDBCardText>
        <MDBDropdown>
          <MDBDropdownToggle color="primary">
            Options
          </MDBDropdownToggle>
          <MDBDropdownMenu>
            {dropdownLinks.map((link, index) => (
              <MDBDropdownItem key={index}>
                <NavLink className="dropdown-item" to={link.to}>
                  {link.text}
                </NavLink>
              </MDBDropdownItem>
            ))}
          </MDBDropdownMenu>
        </MDBDropdown>
      </MDBCardBody>
    </MDBCard>
  );
}

export default MainPage;
