import React, { useState, useEffect } from 'react';

function RecordSaleForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedAutomobile, setSelectedAutomobile] = useState('');
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [selectedCustomer, setSelectedCustomer] = useState('');
  const [price, setPrice] = useState('');

  // Fetch available automobiles, salespeople, and customers when the component mounts
  useEffect(() => {
    // Fetch automobiles
    fetch('http://localhost:8100/api/automobiles/')
      .then((response) => response.json())
      .then((data) => {
        const availableAutos = data.autos.filter(auto => auto.sold === false);
        setAutomobiles(availableAutos);
      })
      .catch((error) => console.error('Error fetching automobiles:', error));

    // Fetch salespeople
    // Add the URL to fetch salespeople and update the state
    fetch('http://localhost:8100/api/salespeople/')
      .then((response) => response.json())
      .then((data) => {
        setSalespeople(data.salespeople);
      })
      .catch((error) => console.error('Error fetching salespeople:', error));

    // Fetch customers
    // Add the URL to fetch customers and update the state
    fetch('http://localhost:8100/api/customers/')
      .then((response) => response.json())
      .then((data) => {
        setCustomers(data.customers);
      })
      .catch((error) => console.error('Error fetching customers:', error));
  }, []);

  // Handle form submission
  const handleSubmit = (event) => {
    event.preventDefault();
    // Implement the logic to send POST request to the server
    const saleData = {
      automobile: selectedAutomobile,
      salesperson: selectedSalesperson,
      customer: selectedCustomer,
      price: price
    };

    fetch('http://localhost:8100/api/sales/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(saleData),
    })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Something went wrong');
      }
    })
    .then((data) => {
      console.log('Sale recorded:', data);
      // Reset form or give user feedback
    })
    .catch((error) => {
      console.error('Error recording the sale:', error);
    });
  };

  // Handlers for form inputs
  const handleAutomobileChange = (event) => {
    setSelectedAutomobile(event.target.value);
  };

  const handleSalesPersonChange = (event) => {
    setSelectedSalesperson(event.target.value);
  };

  const handleCustomerChange = (event) => {
    setSelectedCustomer(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  // JSX for the form
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale!</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <select onChange={handleAutomobileChange} value={selectedAutomobile} required name="automobile" className="form-select">
                <option value="">Choose an automobile</option>
                {automobiles.map(auto => (
                  <option key={auto.import_href} value={auto.import_href}>
                    {auto.vin}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalesPersonChange} value={selectedSalesperson} required name="sales_person" className="form-select">
                <option value="">Choose a sales person</option>
                {salespeople.map(salesPerson => (
                  <option key={salesPerson.id} value={salesPerson.name}>
                    {salesPerson.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleCustomerChange} value={selectedCustomer} required name="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => (
                  <option key={customer.id} value={customer.name}>
                    {customer.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" className="form-control" />
              <label htmlFor="price">Price (enter without $ sign)</label>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RecordSaleForm;
