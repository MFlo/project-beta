import React, { useState, useEffect } from 'react';

const SalespersonHistory = () => {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesPerson, setSelectedSalesPerson] = useState('');
  const [salesHistory, setSalesHistory] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  // Fetch salespeople when the component mounts
  useEffect(() => {
    setIsLoading(true);
    fetch('/api/salespeople/')
      .then(response => response.json())
      .then(data => {
        setSalespeople(data.salespeople);
        setIsLoading(false);
      })
      .catch(error => {
        setError(error);
        setIsLoading(false);
      });
  }, []);

  // Fetch sales history when a salesperson is selected
  useEffect(() => {
    if (selectedSalesPerson) {
      setIsLoading(true);
      fetch(`/api/sales?salespersonId=${selectedSalesPerson}`)
        .then(response => response.json())
        .then(data => {
          setSalesHistory(data.sales);
          setIsLoading(false);
        })
        .catch(error => {
          setError(error);
          setIsLoading(false);
        });
    }
  }, [selectedSalesPerson]);

  // Handler for dropdown change
  const handleSalesPersonChange = (event) => {
    setSelectedSalesPerson(event.target.value);
  };

  return (
    <div>
      {/* Dropdown to select salesperson */}
      <select onChange={handleSalesPersonChange} value={selectedSalesPerson}>
        <option value="">Select a Salesperson</option>
        {salespeople.map(salesperson => (
          <option key={salesperson.id} value={salesperson.id}>
            {salesperson.firstName} {salesperson.lastName}
          </option>
        ))}
      </select>

      {/* Sales history table */}
      {isLoading && <p>Loading...</p>}
      {error && <p>Error: {error.message}</p>}
      {salesHistory.length > 0 && (
        <table>
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Automobile VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {salesHistory.map(sale => (
              <tr key={sale.id}>
                <td>{`${sale.salesperson.firstName} ${sale.salesperson.lastName}`}</td>
                <td>{sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default SalespersonHistory;
