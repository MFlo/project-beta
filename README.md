# CarCar

Team:

* Ryan Tran - Services?
* Michael Flores - Sales
## Getting Started - Before you get started you will need to make sure you have the following.
1. VSCode
2. Git
3. Docker
4. Insomnia
5. Node.js >= 18.2
complete the following task to get started.
A. Fork this repository https://gitlab.com/MFlo/project-beta.git
B. Clone the forked repository onto your computer
C. Build and run the Docker container with these commands from the project root directory.  Open docker desktop.  Then open a terminal.  From the terminal type these commands from the project-beta root directory

docker volume create beta-data
docker-compose build
docker-compuse up

You can view the project in the browser
http://localhost:3000/

## Design
CarCar is made up of three microservices
1. Inventory
2. Services
3. Sales
## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice
The sales microservice was designed to interact with the Inventory with real time information.
When an automobile is added the sales person and customer will be able to see it.  The sales enduser function allows you to be able to do the following.
1. Create Salespersons
2. List Salespersons
3. List Salesperson sales history
4. Create Customers
5. List Customers
6. Create a sales record
7. List all sales records
Each list also has the function of deleting any entry for continuity

The integration and operation of the application and microservices are based on a Django back-end, SQL Database manged by Docker, and React fron-end.  We will explain the function and process of each step starting with the Django back-end and moving to the React front-end.

-----BACK-END----
Django Rest APi structure

***MODELS***
The back-end has been set up M,V,U,E.  Model - Views - URLS - Encoders.
The models set upto to interact with the inventory are class based.
sales_rest.models.py
- AutomobileVO
- Salesperson
- Customer
- Sale
All set up with CORS (cross origin resource sharing) in mind as the project-beta application reacts with the inventory database adding information and retrieving information with all three microservices.
additional information can be found here.
https://docs.djangoproject.com/en/5.0/topics/db/models/
https://docs.djangoproject.com/en/5.0/ref/models/
https://docs.djangoproject.com/en/5.0/faq/models/
***VIEWS***
The views methods are designed with CRUD (Create, Read, Update, Delete) These are the following API endpoints that allow the end user on the front-end CRUD capablities.  These are the views associated with the models and URLs.
    - salespeople_view,
    - salesperson_delete_view,
    - customers_view,
    - customer_delete_view,
    - sales_view,
    - sale_delete_view,
    - automobiles_view,

Action	                        Method	URL
----------------------------------------------
List salespeople	            GET	    http://localhost:8090/api/salespeople/
Create a salesperson            POST	http://localhost:8090/api/salespeople/
Delete a specific salesperson	DELETE	http://localhost:8090/api/salespeople/:id/
List customers	                GET	    http://localhost:8090/api/customers/
Create a customer	            POST	http://localhost:8090/api/customers/
Delete a specific customer	    DELETE	http://localhost:8090/api/customers/:id/
List sales	                    GET	    http://localhost:8090/api/sales/
Create a sale	                POST	http://localhost:8090/api/sales/
Delete a sale	                DELETE	http://localhost:8090/api/sales/:id
------------------------------------------------
Additional information can be found here.
https://docs.djangoproject.com/en/5.0/ref/class-based-views/base/
https://docs.djangoproject.com/en/5.0/topics/http/views/
https://docs.djangoproject.com/en/5.0/topics/class-based-views/
https://docs.djangoproject.com/en/5.0/topics/http/decorators/

***URLS***
The urls have been set up according to the API endpoints and database structure.
# URL endpoints for salespeople
    path("salespeople/", salespeople_view, name="list_salespeople"),
    path("salespeople/<int:id>/", salesperson_delete_view, name="delete_salesperson"),

# URL endpoints for customers
    path("customers/", customers_view, name="list_customers"),
    path("customers/<int:id>/", customer_delete_view, name="delete_customer"),

# URL endpoints for sales
    path("sales/", sales_view, name="list_sales"),
    path("sales/<int:id>/", sale_delete_view, name="delete_sale"),

# URL endpoint for listing automobiles
    path("automobiles/", automobiles_view, name="list_automobiles"),

Additional information on Django and URLs can  be found here.
https://docs.djangoproject.com/en/5.0/topics/http/urls/

***Encoders***
The encoders for the models.py are in a seperate file.
sales_rest/encoders.py they will allow the views to serialize "translating" models into other formats in this case JSON for use by the front-end.  The ModelEncoders created are the following

- AutomobileVOEncoder
- SalesPersonEncoder
- CustomerEncoder
- SaleEncoder

Additional information can be found at the following website.
https://docs.djangoproject.com/en/5.0/topics/serialization/

For additional explanation consult the documentation for Django and React at the following sites.

DJANGO - https://www.djangoproject.com/

REACT - https://legacy.reactjs.org/docs/getting-started.html
or
REACT QUICK START - https://react.dev/learn
