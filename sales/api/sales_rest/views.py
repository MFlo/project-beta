from django.http import JsonResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import AutomobileVOEncoder, SalesPersonEncoder, CustomerEncoder, SaleEncoder
from django.core.exceptions import ObjectDoesNotExist
import json

@require_http_methods(["GET", "POST"])
def salespeople_view(request):
    if request.method == 'GET':
        salespeople = Salesperson.objects.all()
        return JsonResponse({'salespeople': salespeople}, encoder=SalesPersonEncoder)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            new_salesperson = Salesperson.objects.create(**data)
            return JsonResponse(new_salesperson, encoder=SalesPersonEncoder, safe=False)
        except ValueError as e:
            return JsonResponse({'error': str(e)}, status=400)

@require_http_methods(["DELETE"])
def salesperson_delete_view(request, id):
    try:
        salesperson = Salesperson.objects.get(id=id)
        salesperson.delete()
        return JsonResponse({'message': "Salesperson deleted"})
    except ObjectDoesNotExist:
        return JsonResponse({'error': "Salesperson does not exist"}, status=404)

@require_http_methods(["GET", "POST"])
def customers_view(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse({'customers': customers}, encoder=CustomerEncoder)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            new_customer = Customer.objects.create(
                first_name=data.get('first_name', ''),
                last_name=data.get('last_name' ''),
                address=data.get('address'),
                phone_number=data.get('phone_number')
            )
            return JsonResponse(new_customer, encoder=CustomerEncoder, safe=False)
        except ValueError as e:
            return JsonResponse({'error': str(e)}, status=400)

@require_http_methods(["DELETE"])
def customer_delete_view(request, id):
    try:
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse({'message': "Customer successfully deleted"})
    except Customer.DoesNotExist:
        return HttpResponseNotFound({'error': "Customer does not exist"})

@require_http_methods(["GET", "POST"])
def sales_view(request):
    if request.method == 'GET':
        if 'vin' in request.GET:
            vin = request.GET['vin']
            sales = Sale.objects.filter(automobile__vin=vin, automobile__sold=True)
        else:
            sales = Sale.objects.filter(automobile__sold=True)
        return JsonResponse({'sales': sales}, encoder=SaleEncoder)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            automobile = AutomobileVO.objects.get(vin=data['automobile'], sold=False)
            salesperson = Salesperson.objects.get(employee_id=data['salesperson'])
            customer = Customer.objects.get(id=data['customer'])

            new_sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=data['price']
            )
            automobile.sold = True
            automobile.save()

            return JsonResponse(new_sale, encoder=SaleEncoder, safe=False)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, status=404)
        except ValueError as e:
            return JsonResponse({'error': str(e)}, status=400)

@require_http_methods(["DELETE"])
def sale_delete_view(request, id):
    try:
        sale = Sale.objects.get(id=id)
        sale.delete()
        return JsonResponse({'message': "Sale deleted"})
    except ObjectDoesNotExist:
        return JsonResponse({'error': "Sale does not exist"}, status=404)

@require_http_methods(["GET"])
def automobiles_view(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse({'automobiles': automobiles}, encoder=AutomobileVOEncoder)
