from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, default='')  # Assuming VIN is a string
    sold = models.BooleanField(default=False)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField(unique=True)

class Customer(models.Model):
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=15)

class Sale(models.Model):
    salesperson = models.ForeignKey(Salesperson, on_delete=models.CASCADE, default='')
    automobile = models.ForeignKey(AutomobileVO, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
