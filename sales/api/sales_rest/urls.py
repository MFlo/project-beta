from django.urls import path
from .views import (
    salespeople_view,
    salesperson_delete_view,
    customers_view,
    customer_delete_view,
    sales_view,
    sale_delete_view,
    automobiles_view,
)

urlpatterns = [
    # URL endpoints for salespeople
    path("salespeople/", salespeople_view, name="list_salespeople"),
    path("salespeople/<int:id>/", salesperson_delete_view, name="delete_salesperson"),

    # URL endpoints for customers
    path("customers/", customers_view, name="list_customers"),
    path("customers/<int:id>/", customer_delete_view, name="delete_customer"),

    # URL endpoints for sales
    path("sales/", sales_view, name="list_sales"),
    path("sales/<int:id>/", sale_delete_view, name="delete_sale"),

    # URL endpoint for listing automobiles
    path("automobiles/", automobiles_view, name="list_automobiles"),
]
