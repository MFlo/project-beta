import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

API_URL = 'http://project-beta-inventory-api-1:8000/api/automobiles/'

def process_autos_data(autos_data):
    for auto_data in autos_data:
        vin = auto_data.get('vin', '')
        sold = auto_data.get('sold', False)
        AutomobileVO.objects.update_or_create(vin=vin, defaults={'sold': sold})

def poll(testing=False):
    while True:
        print('Service poller polling for data')
        try:
            if testing:
                response_data = {
                    "autos": [
                        {"href": "/api/automobiles/1/", "vin": "1", "sold": True},
                        {"href": "/api/automobiles/2/", "vin": "2", "sold": True},
                        {"href": "/api/automobiles/3/", "vin": "3", "sold": True},
                    ]
                }
                process_autos_data(response_data["autos"])
            else:
                response = requests.get(API_URL)
                if response.status_code == 200:
                    content = response.json()
                    process_autos_data(content.get('autos', []))
                else:
                    print(f"Failed to fetch data. Status code: {response.status_code}")
        except Exception as e:
            print(f"Error: {e}", file=sys.stderr)

        time.sleep(60)  # Poll every 60 seconds

if __name__ == "__main__":
    poll(testing=False)  # Set to True for testing
